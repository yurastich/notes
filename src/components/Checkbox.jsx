import React, { Component } from "react";


class Checkbox extends Component {
  constructor(props) {
    super(props);

    const { id, name, label, checked } = props;

    this.state = {
      id: id || "",
      name: name || label,
      checked: checked ? true : false
    };
  }

  handleChange = e => {
    const { callback } = this.props;

    this.setState({ checked: e.target.checked });

    if (callback && typeof callback === "function") {
      callback(e.target.checked);
    }
  };

  render() {
    const { state, props, handleChange } = this;
    const { id, name, checked } = state;
    const { label } = props;
    return (
      <div className="field">
        <label className="check">
          <input
            id={id}
            name={name.toLowerCase()}
            checked={checked}
            ref={el => (this.ref = el)}
            onChange={handleChange}
            type="checkbox"
            className="check__input"
          />
          <span className="check__view" />
          <label htmlFor={id} className="check__description">
            {label}
          </label>
        </label>
      </div>
    );
  }
}

export default Checkbox;
