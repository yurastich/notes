import React, { Component } from 'react';
import NotesCtrl from '../controllers/NotesCtrl';
import { Simplex } from 'react-simplex';


class Comment extends Component {

  delete = (id) => {
    const { note_id } = this.props;

    Simplex.set('Loader', true);

    NotesCtrl.removeComment(note_id, id).then(response => {
      Simplex.set('Loader', false);
      Simplex.set('Success', 'Comment deleted');
    })
  }

  render() {
    const { data } = this.props;
    return (
      <div className="comment">
        <div className="comment__header">
          <span className="comment__author">
            {data.author}
          </span>
          <span className="comment__date">{NotesCtrl.toUnix(data.created_at)}</span>
          <button
            className="btn-icon btn-icon--close"
            onClick={() => this.delete(data.id)}
          >
            <i className="fa fa-times"></i>
          </button>
        </div>
        <div className="comment__content">
          {data.content}
        </div>
      </div>
    )
  }
}

export default Comment;