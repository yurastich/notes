import React, { Component } from 'react';
import NotesCtrl from '../controllers/NotesCtrl';
import Input from "../components/Input";
import { Simplex } from "react-simplex";


class CommentCreate extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      commentError: false
    }
  }

  createComment = (e) => {
    e.preventDefault();

    
    const { id } = this.props;
    const data = {
      author: this.author.state.value,
      content: this.content.state.value
    };
    if (
      data.content.length > 0
      && data.author.length > 0
    ) {
      Simplex.set('Loader', true);
      NotesCtrl.createComment(id, data).then(response => {
        this.author.state.value = '';
        this.content.state.value = '';

        Simplex.set('Loader', false);
        Simplex.set('Success', 'Comment created');
        
        this.setState({
          commentError: false
        })
      })
    } else {
      this.setState({
        commentError: true
      })
    }
  }


  render() {
    const { commentError } = this.state;
    return (
      <form onSubmit={this.createComment}>
        <Input
          label="Your name"
          ref={el => (this.author = el)}
        />
        <div className="field-send">
          <Input
            label="New comment"
            ref={el => (this.content = el)}
          />
          <button
            type="submit"
            className="btn-icon field-send__btn"
          >
            <i className="fa fa-paper-plane" />
          </button>
        </div>
        {commentError && (
          <p className="text text--error">This fields is required</p>
        )}
      </form>
    )
  }
}

export default CommentCreate;