import React, { Component } from "react";
import Loader from './Loader'
import { Simplex, SimplexMapToProps } from 'react-simplex';


class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {
      successText: props.success
    }

  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      successText: nextProps.success
    })

    this.hideSuccess();
  }

  hideSuccess = () => {

    setTimeout(() => {
      Simplex.set('Success', '');
    }, 2000)

  }

  render() {
    const { successText } = this.state;
    const { title, success } = this.props;
    return (
      <div className="header">
        <div className="container--fluid">
          <div className="row">
            <div className="col-xs-12">
              <div className="header__title">
                {title || "Header"}
              </div>
            </div>
            <div className={`header__success ${success !== '' ? 'header__success--show' : ''}`}>
              {successText}
            </div>
          </div>
        </div>
        <Loader />
      </div>
    )
  }
}

export default SimplexMapToProps(Header, (storage) => {
  return {
    success: storage.Success
  };
});
