import React, { Component } from "react";


class Input extends Component {
  constructor(props) {
    super(props);

    const { value, name, label } = props;

    this.state = {
      value: value || "",
      name: name || label
    };
  }

  handleChange = e => {
    this.setState({ value: e.target.value });
  };

  render() {
    const { name, value } = this.state;
    const {
      type,
      label,
      right,
      classes,
      textarea
    } = this.props;

    const attrs = {
      name: name.toLowerCase(),
      placeholder: label,
      ref: el => (this.ref = el),
      value: value,
      type: type || "text",
      className: `field__input ${value.length > 0 ? "is-focus" : ""}`,
      autoComplete: `react-${name.toLowerCase()}`
    };

    return (
      <div
        className={`field 
        ${right ? "field--right" : ""} 
        ${classes || ""}
        `}
      >
        {!textarea && <input {...attrs} onChange={this.handleChange} />}

        {textarea && (
          <textarea {...attrs} rows="6" onChange={this.handleChange} />
        )}

        <span className="field__label">{label}</span>
      </div>
    );
  }
}

export default Input;
