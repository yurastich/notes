import React, { Component } from 'react';
import { SimplexMapToProps } from 'react-simplex';

class Loader extends Component {
  render() {
    return (
      <div className={`loader ${this.props.loader ? 'loader--show' : ''}`}>
        <div className="blobs">
          <div className="blob-center"></div>
          <div className="blob"></div>
          <div className="blob"></div>
          <div className="blob"></div>
          <div className="blob"></div>
          <div className="blob"></div>
          <div className="blob"></div>
        </div>
      </div>
    )
  }
}


export default SimplexMapToProps(Loader, (storage) => {
  return {
    loader: storage.Loader
  };
});
