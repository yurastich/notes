import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Simplex, SimplexMapToProps } from 'react-simplex';
import Checkbox from "./Checkbox";
// import { Simplex, SimplexMapToProps } from 'react-simplex';

class Sidebar extends Component {
  constructor(props) {
    super(props);

    const { localStorage } = props;

    this.state = {
      check: data => {
        Simplex.set("LocalStorage", data);
      },
      localStorage: localStorage
    };

  }

  render() {
    const { localStorage, check } = this.state;
    const { notes } = this.props;

    return (
      <div className="sidebar">
        <div className="sidebar__container">
          <ul className="sidebar__list">
            <li className="sidebar__item">
              <NavLink
                to="/"
                exact
                className="sidebar__link"
                activeClassName="is-active"
              >
                Create
              </NavLink>
            </li>
            {notes.length > 0 &&
              <li className="sidebar__item">
                <NavLink
                  to="/notes"
                  className="sidebar__link"
                  activeClassName="is-active"
                >
                  Notes
              </NavLink>
              </li>
            }
            <li className="sidebar__item">
              <div className="sidebar__link">
                <Checkbox
                  label="Local Storage"
                  id="local"
                  checked={localStorage}
                  ref={el => (this.local = el)}
                  callback={data => check(data)}
                />
              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default SimplexMapToProps(Sidebar, (storage) => {
  const { LocalStorage, LocalStorageData, Data } = storage;
  const notesList = LocalStorage ? LocalStorageData.notes : Data.notes;

  return {
    localStorage: LocalStorage,
    notes: notesList
  };
});
