import React, { Component } from "react";
import Modal from "./Modal";

class ModalBoolean extends Component {

  constructor(props) {
    super(props);

    const { show, text } = props;

    this.state = {
      showModal: show || false,
      text: text || ""
    };
  }

  componentWillReceiveProps(nextProp) {
    const { show, success } = nextProp;

    this.setState({
      showModal: show,
      success: success
    });
  }

  handleClose = () => {
    const { onRequestClose } = this.props;

    this.setState({
      showModal: false
    });

    if (typeof onRequestClose === "function") {
      onRequestClose()
    }
  }

  handleSuccess = () => {
    const { success } = this.state;
    success();
  }

  model() {

    return (
      <div className="modal__model">
        <p className="text">{this.state.text}</p>
        <div className="btn-line">
          <button onClick={this.handleSuccess} className="btn">
            Yes
          </button>
          <button onClick={this.handleClose} className="btn">
            No
          </button>
        </div>
      </div>

    )
  }

  render() {
    return (
      <Modal
        {...this.props} model={this.model()}
      />
    );
  }
}

export default ModalBoolean;
