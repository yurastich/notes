import React, { Component } from "react";
import { Simplex } from "react-simplex";
import NotesCtrl from "../../controllers/NotesCtrl";

import Modal from "./Modal";
import Input from "../Input";

class ModalNoteEdit extends Component {
  constructor(props) {
    super(props);

    const { show, note } = props;

    this.state = {
      showModal: show || false,
      editError: false,
      note: note
    };
  }

  componentWillReceiveProps(nextProp) {
    const { show, note } = nextProp;

    this.setState({
      showModal: show,
      note: note
    });
  }

  handleClose = () => {
    const { onRequestClose } = this.props;

    this.setState({
      showModal: false
    });

    if (typeof onRequestClose === "function") {
      onRequestClose()
    }
  }

  handleSuccess = e => {
    e.preventDefault();

    const { state, handleClose } = this;
    const { note } = state;

    note.author = this.author.state.value;
    note.content = this.content.state.value;

    if (note.content.length > 0 && note.author.length > 0) {
      Simplex.set("Loader", true);

      NotesCtrl.editNote(note).then(response => {
        this.author.state.value = "";
        this.content.state.value = "";

        handleClose();

        Simplex.set("Loader", false);
        Simplex.set("Success", "Note is edited");

        this.setState({
          commentError: false
        });
      });
    } else {
      this.setState({
        commentError: true
      });
    }
  }

  model() {
    const { state, handleSuccess, handleClose } = this;
    const { note, editError } = state;

    return (
      <div className="modal__model">
        <Input
          label="Author"
          value={note.author}
          ref={el => (this.author = el)}
        />
        <Input
          label="Note"
          value={note.content}
          textarea
          ref={el => (this.content = el)}
        />
        {editError && (
          <p className="text text--error">This fields is required</p>
        )}
        <div className="btn-line">
          <button onClick={handleSuccess} className="btn">
            Edit
          </button>
          <button onClick={handleClose} className="btn">
            Cancel
          </button>
        </div>
      </div>
    );
  }

  render() {
    return <Modal {...this.props} model={this.model()} large />;
  }
}

export default ModalNoteEdit;
