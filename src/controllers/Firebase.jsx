import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyARgtP9FjuF3BHiS216ltZe930bRL5JEnU",
  authDomain: "notes-karpenko.firebaseapp.com",
  databaseURL: "https://notes-karpenko.firebaseio.com",
  projectId: "notes-karpenko",
  storageBucket: "notes-karpenko.appspot.com",
  messagingSenderId: "510082168346"
};

firebase.initializeApp(config);

export const db = firebase.database();