import { db } from "./Firebase";
import uuid from "uuid/v4";
import moment from "moment";
import { Simplex } from 'react-simplex';


class NotesCtrl {

  static getAll() {
    return new Promise(resolve => {
      if (!Simplex.Storage.LocalStorage) {
        db.ref('notes').on('value', function (snapshot) {
          const response = snapshot.val();
          let notes = [];

          if (typeof notes === 'object') {
            for (let key in response) {
              notes.push(response[key]);
            }
          } else {
            notes = response;
          }

          Simplex.update("Data", {
            notes: notes
          });

          resolve({
            error: false,
            response: notes
          });
        });
      } else {
        const storageNotes = Simplex.Storage.LocalStorageData.notes;
        
        resolve({
          error: false,
          response: storageNotes
        });
      }
    })
  }

  static createNote(data) {
    return new Promise(resolve => {
      const id = uuid();
      const note = {
        id: id,
        author: data.author,
        content: data.content,
        created_at: NotesCtrl.timestamp()
      };

      if (Simplex.Storage.LocalStorage) {
        const storageNotes = Simplex.Storage.LocalStorageData.notes;
        Simplex.update("LocalStorageData", {
          notes: [...storageNotes, note]
        });

        resolve({
          error: false,
          response: note
        })
      } else {
        db.ref(`notes/${id}`).set(note).then(success => {
          resolve({
            error: false,
            response: note
          })
        });
      }


    })
  }

  static editNote(note) {
    return new Promise(resolve => {
      note.updated_at = NotesCtrl.timestamp();
      if (Simplex.Storage.LocalStorage) {
        const storageNotes = Simplex.Storage.LocalStorageData.notes;

        storageNotes.find((storageNote, key) => {
          if (storageNote.id === note.id) {
            return storageNotes[key] = note;
          } else {
            return false
          }
        });

        Simplex.update("LocalStorageData", {
          notes: [...storageNotes]
        });

        resolve({
          error: false,
          response: note
        })
      } else {
        db.ref(`notes/${note.id}`).update(note).then(success => {
          resolve({
            error: false,
            response: note
          })
        });
      }
    })
  }

  static removeNote(id) {
    return new Promise(resolve => {
      if (Simplex.Storage.LocalStorage) {
        const storageNotes = Simplex.Storage.LocalStorageData.notes;

        storageNotes.find((storageNote, key) => {
          if (storageNote.id === id) {
            return storageNotes.splice(key, 1);
          } else {
            return false
          }
        });

        Simplex.update("LocalStorageData", {
          notes: [...storageNotes]
        });

        resolve({
          error: false
        })
      } else {
        db.ref(`notes/${id}`).remove().then(success => {
          resolve({
            error: false,
            response: success
          })
        });
      }
    })
  }



  static createComment(note_id, data) {
    return new Promise(resolve => {
      const id = uuid();
      const comment = {
        id: id,
        author: data.author,
        content: data.content,
        created_at: NotesCtrl.timestamp(),
      };

      if (Simplex.Storage.LocalStorage) {
        const storageNotes = Simplex.Storage.LocalStorageData.notes;

        storageNotes.find((storageNote, note_key) => {
          if (storageNote.id === note_id) {
            console.log("storageNotes ", storageNotes)
            if (!storageNotes[note_key].comments) {
              storageNotes[note_key].comments = []
            }
            return storageNotes[note_key].comments.push(comment)
          } else {
            return false
          }
        });

        Simplex.update("LocalStorageData", {
          notes: [...storageNotes]
        });

        resolve({
          error: false
        })
      } else {
        db.ref(`notes/${note_id}/comments/${id}`).update(comment).then(success => {
          resolve({
            error: false,
            response: comment
          })
        });
      }
    })
  }

  static removeComment(note_id, comment_id) {
    return new Promise(resolve => {
      if (Simplex.Storage.LocalStorage) {
        const storageNotes = Simplex.Storage.LocalStorageData.notes;

        storageNotes.find((storageNote, note_key) => {
          if (storageNote.id === note_id) {
            return storageNote.comments.find(
              (comment, comment_key) => {
                if (comment.id === comment_id) {
                  return storageNotes[note_key].comments.splice(comment_key, 1);
                } else {
                  return false
                }
              }
            )
          } else {
            return false
          }
        });

        Simplex.update("LocalStorageData", {
          notes: [...storageNotes]
        });

        resolve({
          error: false
        })
      } else {
        db.ref(`notes/${note_id}/comments/${comment_id}`).remove().then(success => {
          resolve({
            error: false,
            response: success
          })
        });
      }
    })
  }

  static timestamp() {
    return moment(moment().format()).format("X")
  }

  static toUnix(date) {
    return moment.unix(date || "").format("DD.MM.YYYY")
  }

}

export default NotesCtrl;