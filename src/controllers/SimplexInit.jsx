import { Simplex } from 'react-simplex';
import NotesCtrl from './NotesCtrl';


const SimplexInit = () => {


  Simplex.setStorageDriver(
    localStorage, // storage driver
    false         // driver is not async
  )

  window.Simplex = Simplex;

  Simplex.init('LocalStorage', false, true);
  Simplex.init('LocalStorageData', {
    notes: []
  }, true);
  Simplex.init('Loader', false, false);
  Simplex.init('Success', '', false);
  Simplex.init('Data', {
    notes: []
  }, true);

  // Get notes from Firebase and write to localStorage
  NotesCtrl.getAll().then(resolve => {

  })


}

export default SimplexInit;