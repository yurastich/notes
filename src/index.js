import React from 'react';
import ReactDOM from 'react-dom';
import './style/style.scss';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import SimplexInit from './controllers/SimplexInit';

// Pages
import NotesPage from './pages/NotesPage';
import NotePage from './pages/NotePage';
import CreatePage from './pages/CreatePage';
// END Pages

SimplexInit();

ReactDOM.render(
  <Router>
    <Switch>

      <CreatePage exact path='/' />
     
      <NotePage path='/notes/:id' />
      <NotesPage path='/notes' />

    </Switch>
  </Router>,
  document.getElementById('root')
)