import React from 'react';
import Input from '../components/Input'
import { Simplex } from 'react-simplex';
import NotesCtrl from '../controllers/NotesCtrl';


class Create extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: false,
      loader: false,
      created: false,
    }

  }

  hundleSubmit = (e) => {
    e.preventDefault();

    const data = {
      author: this.author.state.value,
      content: this.content.state.value
    };

    Simplex.set('Loader', true);

    this.setState({
      created: true
    });

    if (
      data.content.length > 0
      && data.author.length > 0
    ) {
      NotesCtrl.createNote(data).then(response => {
        this.author.state.value = '';
        this.content.state.value = '';

        Simplex.set('Loader', false);
        Simplex.set('Success', 'Created');
        this.setState({
          created: false
        })
      })
    } else {
      this.setState({
        error: true
      })
    }
  }

  render() {
    return (
      <div className="page__content">
        <form onSubmit={this.hundleSubmit}>
          <div className="row">
            <div className="col-lg-12 col-sm-12 col-xs-12">
              <Input
                label="New note"
                textarea
                ref={el => this.content = el}
              />
            </div>
            <div className="col-lg-12 col-sm-12 col-xs-12">
              <Input
                label="Name"
                ref={el => this.author = el}
              />
            </div>
          </div>

          {this.state.error && (
            <p className="text text--error">This fields is required</p>
          )}

          <button type="submit" className="btn">
            Create
          </button>

        </form>

      </div>
    )
  }
}


export default Create;
