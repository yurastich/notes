import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { Simplex, SimplexMapToProps } from "react-simplex";
import NotesCtrl from "../controllers/NotesCtrl";
import CommentCreate from "../components/CommentCreate";
import Comment from "../components/Comment";
import ModalBoolean from "../components/modals/ModalBoolean";
import ModalNoteEdit from "../components/modals/ModalNoteEdit";

class Note extends Component {
  constructor(props) {
    super(props);

    this.state = {
      note: {
        author: "",
        created_at: "",
        content: ""
      },
      modalDeleteNoteShow: false,
      modalEditNoteShow: false,
      onCloseModal: () => {
        this.setState({
          modalDeleteNoteShow: false,
          modalEditNoteShow: false
        });
      }
    };
  }

  componentDidMount() {
    const { id, notes } = this.props;

    if (notes.length > 0) {
      this.updateNote(id, notes);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { id, notes } = nextProps;

    this.updateNote(id, notes);
  }

  updateNote = (id, notes) => {
    this.setState({
      note: notes.find(note => note.id === id)
    });
  };

  delete = id => {
    Simplex.set("Loader", true);
    NotesCtrl.removeNote(id).then(response => {
      Simplex.set("Success", "Note deleted");
      Simplex.set("Loader", false);
    });
  };

  handleDelete = id => {
    this.setState({
      modalDeleteNoteShow: true,
      modalDeleteAction: () => {
        this.delete(id);
      }
    });
  };

  handleEdit = () => {
    this.setState({
      modalEditNoteShow: true,
      modalEditAction: () => {
        alert("edit");
      }
    });
  };

  render() {
    const {
      note,
      modalDeleteNoteShow,
      modalEditNoteShow,
      onCloseModal,
      modalDeleteAction,
      modalEditAction
    } = this.state;

    const comments = [];

    if (!note) {
      return <Redirect to="/notes" />;
    }

    if (note && typeof note.comments === "object" && note.comments) {
      for (let key in note.comments) {
        comments.push(note.comments[key]);
      }
    }

    return (
      <div className="note-detail">
        <div className="note-detail__header">
          <div className="note-detail__header__info">
            <div className="note-detail__author">{note.author || ""}</div>
            <div className="note-detail__date">
              {NotesCtrl.toUnix(note.created_at)}
            </div>
          </div>
          <div className="btn-line">
            <button className="btn-icon" onClick={this.handleEdit}>
              <i className="fa fa-pencil" />
            </button>
            <button
              className="btn-icon"
              onClick={() => this.handleDelete(note.id)}
            >
              <i className="fa fa-trash-o" />
            </button>
          </div>
        </div>
        <p className="note-detail__text">{note.content}</p>
        <div className="note-detail__comments">
          <h2 className="title">Comments</h2>
          <CommentCreate id={note.id} />
          {comments.length > 0 &&
            comments.map(comment => (
              <Comment
                key={comment.id}
                data={comment}
                note_id={note.id}
              />
            ))}
        </div>
        <ModalBoolean
          text="Delete note?"
          isOpen={modalDeleteNoteShow}
          onRequestClose={onCloseModal}
          success={modalDeleteAction}
        />
        <ModalNoteEdit
          note={note}
          isOpen={modalEditNoteShow}
          onRequestClose={onCloseModal}
          success={modalEditAction}
        />
      </div>
    );
  }
}

export default SimplexMapToProps(Note, (storage, current_props) => {
  const { LocalStorage, LocalStorageData, Data } = storage;
  const notesList = LocalStorage ? LocalStorageData.notes : Data.notes;

  return {
    notes: notesList
  };
});
