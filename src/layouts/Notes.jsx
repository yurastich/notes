import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import { Link } from 'react-router-dom';
import { SimplexMapToProps } from 'react-simplex';
import NotesCtrl from '../controllers/NotesCtrl';


class Notes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      notes: props.notes
    }

  }

  componentWillReceiveProps(nextProps) {

    this.setState({
      notes: nextProps.notes
    })
  }

  render() {
    const {
      notes
    } = this.state;

    if (notes.length === 0) {
      return <Redirect to='/' />;
    }

    const items = notes.map(note => (
      <li className="notes-item" key={note.id}>
        <Link to={`/notes/${note.id}`} className="note-detail">
          <div className="note-detail__header">
            <div className="note-detail__author">{note.author}</div>
            <div className="note-detail__date">{NotesCtrl.toUnix(note.created_at)}</div>
          </div>
          <p className="note-detail__text">
            {note.content}
          </p>
        </Link>
      </li>
    ))

    return (
      <ul className="notes-list">
        {items}
      </ul>
    )
  }
}

export default SimplexMapToProps(Notes, storage => {
  const { LocalStorage, LocalStorageData, Data } = storage;
  const notesList = LocalStorage ? LocalStorageData.notes : Data.notes;

  return {
    notes: notesList
  };
});