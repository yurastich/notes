import React from 'react';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import Loader from '../components/Loader';
import Create from '../layouts/Create';


class CreatePage extends React.Component {
  render() {
    return (
      <div className="page">
        <Sidebar />
        <Header title='Create' />
        <div className="page__body">
          <div className="container--fluid">
            <Create />
          </div>
          <Loader />
        </div>
      </div>
    );
  }
}
export default CreatePage;