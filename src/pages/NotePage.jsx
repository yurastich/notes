import React, { Component } from 'react';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import Note from '../layouts/Note';


class NotePage extends Component {
  render() {
    return (
      <div className="page">
        <Sidebar />
        <Header title='Note detail' />
        <div className="page__body">
          <div className="container--fluid">
            <div className="page__content">
              <Note id={this.props.computedMatch.params.id} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default NotePage;