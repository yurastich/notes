import React from 'react';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import Notes from '../layouts/Notes';


class NotesPage extends React.Component {
  render() {
    return (
      <div className="page">
        <Sidebar />
        <Header title='Note list' />
        <div className="page__body">
          <div className="container--fluid">
            <div className="page__content">
              <Notes />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default NotesPage;